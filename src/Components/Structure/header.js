import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

const Header = () => {
  const { i18n } = useTranslation();
  const history = useHistory();

  return (
    <div className="header">
      <span onClick={() => history.push('/')}>Home</span>
      {' '}
      <span onClick={() => history.push('/demo')}>Demo</span>
      {' '}
      <span onClick={() => i18n.changeLanguage('en')}>English</span>
      {' '}
      <span onClick={() => i18n.changeLanguage('zh-hk')}>中文</span>
    </div>
  );
};

export default Header;