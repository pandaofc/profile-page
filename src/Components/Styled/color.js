export const backgroundColor = `#1f2325`;
export const cardBackgroundColor = `#25292b`;
export const primaryColor = `#1f79e1`;
export const textColor = `#848485`;
export const greenColor = `#42b681`;