/* General */
export const ERROR = 'ERROR';

/* Demo */
export const SET_DEMO_REDUCER = 'SET_DEMO_REDUCER';
export const PUSH_DEMO_REDUCER = 'PUSH_DEMO_REDUCER';
export const CLEAR_DEMO_REDUCER = 'CLEAR_DEMO_REDUCER';
