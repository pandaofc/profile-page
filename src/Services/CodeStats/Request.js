import axios from 'axios'

/* eslint-disable */
const token = process.env.CODE_STATS_TOKEN;
const baseAPI = 'https://codestats.net/api/'
const service = axios.create({
  baseURL: baseAPI,
  timeout: 30000,
});

service.interceptors.request.use(
  config => {
    config.headers['X-API-Token'] = token;
    config.headers['access-control-allow-origin'] = '*';
    return config;
  },
  error => Promise.reject(error),
);

service.interceptors.response.use(
  response => {
    const res = response.data;
    if (res.status !== 'success') {
      alert(res.message);
      if (res.status === 'expired') location.reload();
      return Promise.reject('error');
    } else {
      if (res.message !== '') alert(res.message);
      return response.data;
    }
  },
  error => {
    const err = error.response;
    console.log(err);
    alert('Connection Problems');
    return Promise.reject(error)
  }
);

export default service;
