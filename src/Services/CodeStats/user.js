import service from '../CodeStats/Request';

export function profileService() {
  return service({
    url: '/users/username',
    method: 'GET'
  });
}
